package solution;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AlphabetSoupSolver {

    public static void main(String[] args) {

        String inputFile = "C:\\Users\\Public\\WAP\\Labs\\alphabet-soup\\alphabetsolution\\src\\solution\\input.txt";

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            String gridSizeLine = reader.readLine();
            String[] gridSizeParts = gridSizeLine.split("x");
            int numRows = Integer.parseInt(gridSizeParts[0]);
            int numCols = Integer.parseInt(gridSizeParts[1]);

            char[][] grid = new char[numRows][numCols];

            for (int i = 0; i < numRows; i++) {
                String rowLine = reader.readLine();
                String[] rowParts = rowLine.split(" ");
                for (int j = 0; j < numCols; j++) {
                    grid[i][j] = rowParts[j].charAt(0);
                }
            }

            List<String> wordsToFind = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                wordsToFind.add(line);
            }

            for (String word : wordsToFind) {
                findWord(grid, word);
            }

        } catch (IOException e) {
            System.err.println("Error reading the input file: " + e.getMessage());
        }
    }

    private static void findWord(char[][] grid, String word) {
        int numRows = grid.length;
        int numCols = grid[0].length;

        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                if (searchWord(grid, word, row, col)) {
                    int endRow = row + word.length() - 1;
                    int endCol = col + word.length() - 1;
                    System.out.println(word + " " + row + ":" + col + " " + endRow + ":" + endCol);
                    return; // Move on to the next word
                }
            }
        }
    }

    private static boolean searchWord(char[][] grid, String word, int row, int col) {
        int numRows = grid.length;
        int numCols = grid[0].length;
        int len = word.length();

        // Check horizontal right
        if (col + len <= numCols) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row][col + i] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        // Check horizontal left
        if (col - len + 1 >= 0) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row][col - i] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        // Check vertical down
        if (row + len <= numRows) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row + i][col] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        // Check vertical up
        if (row - len + 1 >= 0) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row - i][col] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        // Check diagonal down-right
        if (row + len <= numRows && col + len <= numCols) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row + i][col + i] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        // Check diagonal down-left
        if (row + len <= numRows && col - len + 1 >= 0) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row + i][col - i] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        // Check diagonal up-right
        if (row - len + 1 >= 0 && col + len <= numCols) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row - i][col + i] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        // Check diagonal up-left
        if (row - len + 1 >= 0 && col - len + 1 >= 0) {
            boolean found = true;
            for (int i = 0; i < len; i++) {
                if (grid[row - i][col - i] != word.charAt(i)) {
                    found = false;
                    break;
                }
            }
            if (found) return true;
        }

        return false;
    }
}

